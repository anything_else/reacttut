var gulp = require('gulp');
var babel = require('gulp-babel');
var rename= require('gulp-rename')
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');

gulp.task('default',['brow'], function() {

});

gulp.task('brow',['es5'], function() {
    return browserify('./dist/app.js')
        .transform(babelify)
        .bundle()
        .pipe(source('app.js'))
        .pipe(rename('final.js'))
        .pipe(gulp.dest("./dist"));;
});

gulp.task('es5', function() {
    
    return gulp.src(['src/**/*.js', 'src/**/*.jsx'])
        .pipe(babel())
        .pipe(gulp.dest('dist'));
});