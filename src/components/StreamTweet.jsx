import React from 'react';
import ReactDOM from 'react-dom';
import Header from'./Header';
import Tweet from'./Tweet';

export default class StreamTweet extends React.Component {
    constructor() {
        super();
        this.state = {
            numberOfCharactersIsIncreasing: null,
            headerText: null
        };
    }
    
    render() {
        console.log('[Snapterest] StreamTweet 2: Running render()');

        return (
            <section>
                <Header text={this.state.headerText} />
                <Tweet 
                    tweet={ this.props.tweet }
                    onImageClick={ this.props.onAddTweetToCollection }
                />
            </section>
        );
    }

    componentWillMount() {
        console.log('[Snapterest] StreamTweet 1. Running compponentWillMount()');

        this.setState({
            numberOfCharactersIsIncreasing: true,
            headerText: 'Latest public photo from Twitter'
        });

        window.snapterest = {
            numberOfReceivedTweets: 1,
            numberOfDisplayedTweets: 1
        };
    }

    componentDidMount() {
        console.log('[Snapterest] StreamTweet 3. Running componentDidMount()');

        let componentDOMRepresentation = ReactDOM.findDOMNode(this);
        window.snapterest.headerHTML = componentDOMRepresentation.children[0].outerHTML;
        window.snapterest.tweetHTML = componentDOMRepresentation.children[1].outerHTML;
    }

    componentWillUnmount() {
        console.log('[Snapterest] StreamTweet 7. Running componentWillUnmount()');
        delete window.snapterest;        
    }

    componentWillReceiveProps(nextProps) {
        console.log('[Snapterest] StreamTweet 4. Running componentWillReceiveProps()');
        
        const currentTweetLength = this.props.tweet.text.length;
        const nextTweetLength = nextProps.tweet.text.length;
        const isNumberofCharactersIncreasing = (nextTweetLength > currentTweetLength)
        let headerText;

        if(isNumberofCharactersIncreasing) {
            headerText = 'Number of characters is increasing';
        }
        else {
            headerText = 'Latest public photo from Tweeter';            
        }

        this.setState({
            numberOfCharactersIsIncreasing: isNumberofCharactersIncreasing,
            headerText: headerText
        });

        window.snapterest.numberOfReceivedTweets++;        
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('[Snapterest] StreamTweet 5. Running shouldComponentUpdate()');
        
        return (nextProps.tweet.text.length > 1);
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('[Snapterest] StreamTweet 6. Running componentWillUpdate()');
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('[Snapterest] StreamTweet 7. Running componentDidUpdate()');
        window.snapterest.numberOfDisplayedTweets++;
    }
}