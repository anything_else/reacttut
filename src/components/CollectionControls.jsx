import React from 'react';
import Header from './Header';
import Button from './Button';
import CollectionRenameForm from './CollectionRenameForm';
import CollectionExportForm from './CollectionExportForm';

export default class CollectionControls extends React.Component {
    constructor() {
        super();
        this.state = {
            name: 'new',
            isEditingName: false
        };
        this.getHeaderText = getHeaderText.bind(this);
        this.toggleEditCollectionName = this.toggleEditCollectionName.bind(this);
        this.setCollectionName = this.setCollectionName.bind(this);
    }

    render() {
        if(this.state.isEditingName) {
            return (
                <CollectionRenameForm
                    name = {this.state.name}
                    onChangeCollectionName = {this.setCollectionName}
                    onCancelCollectionNameChange = {this.toggleEditCollectionName}
                />
            );
        }

        return (
            <div>
                <Header text = {this.getHeaderText} />
                <Button 
                    label = "Rename collection"
                    handleClick = {this.toggleEditCollectionName}
                />
                <Button 
                    label = "Empty collection"
                    handleClick = {this.props.onRemoveAllTweetsFromCollection}
                />
                <CollectionExportForm htmlMarkup = {this.props.htmlMarkup} />
            </div>
        );
    }

    getHeaderText() {
        let numberOfTweetsInCollection = this.props.numberOfTweetsInCollection;
        let text = numberOfTweetsInCollection;

        if(numberOfTweetsInCollection === 1) {
            text = text + ' tweet in your';
        }
        else {
            text = text + ' tweets in your';
        }

        return (
            <span>
                {text} <strong>{this.state.name}</strong> collection
            </span>
        );
    }

    toggleEditCollectionName() {
        this.setState({
            isEditingName: !this.state.isEditingName
        });
    }

    setCollectionName(name) {
        this.setState({
            name: name,
            isEditingName: false
        });
    }    
}