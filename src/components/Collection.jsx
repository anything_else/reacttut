import React from 'react';
import ReactDOMServer from 'react-dom/server';
import CollectionControls from './CollectionControls';
import TweetList from './TweetList';
import Header from './Header';

export default class Collection extends React.Component {
    constructor(){
        super();
        this.createHtmlMarkupStringOfTweetList = this.createHtmlMarkupStringOfTweetList.bind(this);
        this.getListOfTweetIds = this.getListOfTweetIds.bind(this);
        this.getNumberOfTweetsInCollection = this.getNumberOfTweetsInCollection.bind(this);
    }

    createHtmlMarkupStringOfTweetList() {
        let htmlString = ReactDOMServer.renderToStaticMarkup(
            <TweetList tweets={this.props.tweets} />
        );
        let htmlMarkup = {
            html: htmlString
        };

        return JSON.stringify(htmlMarkup);
    }

    getListOfTweetIds() {
        return Object.keys(this.props.tweets);
    }

    getNumberOfTweetsInCollection() {
        return this.getListOfTweetIds().length;
    }

    render() {
        let numberOfTweetsInCollection = this.getNumberOfTweetsInCollection();

        if(numberOfTweetsInCollection > 0) {
            let tweets = this.props.tweets;
            let htmlMarkup = this.createHtmlMarkupStringOfTweetList();
            let removeAllTweetsFromCollection = this.props.onRemoveAllTweetsFromCollection;
            let handleRemoveTweetFromCollection = this.props.onRemoveTweetFromCollection
            return (
                <div>
                    <CollectionControls
                        numberOfTweetsInCollection = {numberOfTweetsInCollection}
                        htmlMarkup = {htmlMarkup}
                        onRemoveAllTweetsFromCollection = {removeAllTweetsFromCollection}
                    />
                    <TweetList
                        tweets = {tweets}
                        onRemoveTweetFromCollection = {handleRemoveTweetFromCollection}
                    />
                </div>
            );
        }

        return(
            <Header text="Your Collection is empty" />
        );
    }
} 