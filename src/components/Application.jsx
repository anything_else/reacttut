import React from 'react';
import ReactDOM from 'react-dom';
import Stream from './Stream';
import Collection from './Collection';

export default class Application extends React.Component {
    
    constructor(){
        super();
        this.state = { collectionTweets: {} }; 
        this.addTweetToCollection = this.addTweetToCollection.bind(this);
        this.removeAllTweetsFromCollection = this.removeAllTweetsFromCollection.bind(this);
        this.removeTweetFromCollection = this.removeTweetFromCollection.bind(this);
    }      

    addTweetToCollection(tweet) {
        let collectionTweets = this.state.collectionTweets;
        collectionTweets[tweet.id] = tweet;

        this.setState({
            collectionTweets: collectionTweets
        });
    }

    removeTweetFromCollection(tweet) {
        let collectionTweets = this.state.collectionTweets;
        delete collectionTweets[tweet.id];

        this.setState({
            collectionTweets: collectionTweets
        });       
    }

    removeAllTweetsFromCollection() {
        this.setState({
            collectionTweets: {}
        });
    }

    render() {
        return (
            <div className = "container-fluid">
            <div className = "row">
                <div className = "col-md-4 text-center">
                    <Stream onAddTweetToCollection = { this.addTweetToCollection } />                    
                </div> 
                <div className = "col-md-8">
                    <Collection
                        tweets = { this.state.collectionTweets }
                        onRemoveTweetFromCollection = { this.removeTweetFromCollection }
                        onRemoveAllTweetsFromCollection = { this.onRemoveAllTweetsFromCollection } 
                    />
                </div>
            </div>
        </div>
        );
    }
}